import random

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from customer.models import CustomerTest


class Psychic(models.Model):
    username = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.username} (right in {self.rating}%)"


    @property
    def rating(self):
        guesses = self.guesses.filter(right__isnull=False).count()
        right_guesses = self.guesses.filter(right=True).count()
        if guesses > 0:
            return round(100*right_guesses/guesses)
        return 0



class PsychicGuess(models.Model):
    number = models.PositiveSmallIntegerField(editable=False)
    psychic = models.ForeignKey(Psychic, on_delete=models.CASCADE, related_name='guesses', editable=False)
    customer_test = models.ForeignKey(CustomerTest, on_delete=models.SET_NULL, related_name='guesses', null=True, editable=False)

    right = models.BooleanField(default=False, null=True, editable=False)

    def __str__(self):
        return f"{self.psychic}: {self.number} : {self.right}"


