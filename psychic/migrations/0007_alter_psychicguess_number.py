# Generated by Django 3.2.4 on 2021-06-22 10:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('psychic', '0006_alter_psychicguess_number'),
    ]

    operations = [
        migrations.AlterField(
            model_name='psychicguess',
            name='number',
            field=models.PositiveSmallIntegerField(editable=False),
        ),
    ]
