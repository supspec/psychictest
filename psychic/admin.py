from django.contrib import admin

from psychic.models import Psychic


@admin.register(Psychic)
class PsychicAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'rating')
