import random

from customer.models import CustomerTest
from psychic.models import Psychic, PsychicGuess


def assign_customer_test_to_guess(customer_test):
    """
    Берем созданный пользователем тест, случайным образом выбираем 2 или более экстрасенсов
    и генерим догадки для этого теста
    :param customer_test:
    :return:
    """
    psychics_count = Psychic.objects.all().count()
    print(psychics_count)
    if psychics_count < 2:
        raise RuntimeError("We need at least 2 psychics.")

    psychics = list(Psychic.objects.all())
    random.shuffle(psychics)
    psychics = psychics[:random.randint(2, len(psychics))]

    for psychic in psychics:
        PsychicGuess.objects.create(psychic=psychic, customer_test=customer_test, number=random.randint(10, 99))


def save_guess_result(test_id, number):
    for psychic_guess in PsychicGuess.objects.filter(customer_test_id=test_id):
        if psychic_guess.number == number:
            psychic_guess.right = True
            psychic_guess.save()
        else:
            psychic_guess.right = False
            psychic_guess.save()

