from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import CreateView
from django.views.generic.base import View

from psychic.models import Psychic, PsychicGuess
from .forms import CreateTestForm
from .models import CustomerTest
from .utils import assign_customer_test_to_guess, save_guess_result


def assignPsychic(request):
    return redirect('/')


class HomeView(View):
    def get(self, request):
        history = CustomerTest.objects.prefetch_related('guesses').filter(
            session_hashcode=self.request.session['hashcode']).order_by('-pk')
        # history = CustomerTest.objects.filter(session_hashcode=self.request.session['hashcode'])
        physics = Psychic.objects.prefetch_related('guesses').all()
        form = CreateTestForm()
        return render(request, 'customer/index.html', {
            'history': history,
            'physics': physics,
            'form': form
        })


class CreateTest(CreateView):
    form_class = CreateTestForm
    model = CustomerTest

    # success_url = '/check/'

    def form_valid(self, form):
        self.customer_test = form.save(commit=False)
        self.customer_test.session_hashcode = self.request.session['hashcode']
        response = super().form_valid(form)
        assign_customer_test_to_guess(self.customer_test)
        return response

    def get_success_url(self):
        return reverse('check_page', kwargs={'pk': self.object.pk})


class CheckTest(View):
    def get(self, request, pk):
        customer_test = CustomerTest.objects.get(pk=pk)
        if customer_test.session_hashcode != request.session['hashcode']:
            return '/'
        psychic_guesses = PsychicGuess.objects.select_related('psychic').filter(customer_test=customer_test)
        return render(request, 'customer/check.html', {
            # 'history': history,
            'guesses': psychic_guesses,
            'test_id': pk,
        })

    def post(self, request, pk):
        customer_test = get_object_or_404(CustomerTest, pk=pk)
        try:
            number = int(request.POST['number'])
        except ValueError:
            return redirect('/')
        if customer_test.session_hashcode != request.session['hashcode']:
            return redirect('/')
        customer_test.number = number
        customer_test.save()
        save_guess_result(pk, number)
        return redirect('/')
