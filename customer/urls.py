from django.urls import path
from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='test_page'),
    path('create/', views.CreateTest.as_view(), name='create_page'),
    path('check/<int:pk>', views.CheckTest.as_view(), name='check_page'),
]