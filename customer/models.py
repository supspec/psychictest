from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models


class CustomerTest(models.Model):
    number = models.PositiveSmallIntegerField(validators=[MinValueValidator(0),
                                                          MaxValueValidator(99)], null=True)
    session_hashcode = models.CharField(max_length=200, null=True)

    def get_history(self):
        return self.guesses.filter(right__isnull=False)

    def __str__(self):
        return str(self.number)
