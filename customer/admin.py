from django.contrib import admin
from .models import CustomerTest
from psychic.models import PsychicGuess
# admin.site.register(CustomerTest)

class RelatedGuesses(admin.TabularInline):
    model = PsychicGuess
    extra = 0


@admin.register(CustomerTest)
class CustomerTestAdmin(admin.ModelAdmin):
    list_display = ('id', 'number', 'session_hashcode')

    fields = ('number', 'session_hashcode')
    inlines = [RelatedGuesses,]
